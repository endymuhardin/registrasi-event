create table pengunjung (
    id VARCHAR(36),
    nama VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    PRIMARY KEY(id),
    UNIQUE (email)
);
