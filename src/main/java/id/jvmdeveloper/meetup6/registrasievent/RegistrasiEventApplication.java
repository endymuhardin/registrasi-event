package id.jvmdeveloper.meetup6.registrasievent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegistrasiEventApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistrasiEventApplication.class, args);
	}
}
