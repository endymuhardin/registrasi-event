package id.jvmdeveloper.meetup6.registrasievent.controller;

import id.jvmdeveloper.meetup6.registrasievent.dao.PengunjungDao;
import id.jvmdeveloper.meetup6.registrasievent.entity.Pengunjung;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/pengunjung")
public class PengunjungController {

    @Autowired private PengunjungDao pengunjungDao;

    @GetMapping("/")
    public Page<Pengunjung> lihatDataPengunjung(Pageable page){
        return pengunjungDao.findAll(page);
    }

    @GetMapping("/{id}")
    public Pengunjung findById(@PathVariable("id") Pengunjung p) {
        return p;
    }

}
