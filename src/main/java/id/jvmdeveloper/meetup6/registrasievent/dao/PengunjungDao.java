package id.jvmdeveloper.meetup6.registrasievent.dao;

import id.jvmdeveloper.meetup6.registrasievent.entity.Pengunjung;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PengunjungDao extends PagingAndSortingRepository<Pengunjung, String> {
}
